var navbar1 = $("#navbar");
var prevScrollPos = $(window).scrollTop();
$(window).scroll(function () {
    var currentScrollPos = $(window).scrollTop();
    if (prevScrollPos > currentScrollPos) {
        
    } else {
        navbar1.addClass("hide");
        $('.navbar-collapse').collapse('hide');
    }
    if (currentScrollPos === 0) {
        navbar1.removeClass("hide");
        return; 
    }
    prevScrollPos = currentScrollPos;
});

var navbar = document.querySelector('.navbar');
var icon = document.querySelector('.iphone');
var navbarToggler = document.querySelector('.navbar-toggler');
const isPhone = window.matchMedia("(max-width: 992px)").matches;
const isIPhone = window.matchMedia("(max-width: 992px)").matches && /iPhone/i.test(navigator.userAgent);
if (isIPhone) {
    icon.classList.toggle('custom-logo-icon');
    navbarToggler.addEventListener('click', function () {
        navbar.classList.toggle('navbar-mobile-black');
    });
} else {
    navbarToggler.addEventListener('click', function () {
        navbar.classList.toggle('navbar-mobile-blur');
    });
}

anime.timeline({ loop: true })
    .add({
        targets: '.ml15 .word',
        scale: [14, 1],
        opacity: [0, 1],
        easing: "easeOutCirc",
        duration: 800,
        delay: (el, i) => 800 * i
    }).add({
        targets: '.ml15',
        opacity: 0,
        duration: 1000,
        easing: "easeOutExpo",
        delay: 1000
    });