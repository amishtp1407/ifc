var Common = function () {
    return {
        init: function () {
            var btn = $('#buttonup');
            $(window).scroll(function () {
                btn.addClass('show');
            });

            btn.on('click', function (e) {
                e.preventDefault();
                $('html, body').animate({ scrollTop: 0 }, '300');
            });

            $(window).scroll(function () {
                var scrollPos = $(window).scrollTop();
                var windowHeight = $(window).height();
                var documentHeight = $(document).height();
                if (scrollPos + windowHeight + 1 >= documentHeight) {
                  $('#myFooter').removeClass('d-none');
                } else {
                  $('#myFooter').addClass('d-none');
                }
            });
        }
    }
}();

jQuery(document).ready(function () {
    Common.init();
});