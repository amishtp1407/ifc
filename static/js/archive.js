var Archive = function () {
  return {
    init: function () {
      $(window).scroll(function () {
        var scrollPos = $(document).scrollTop();
        var windowHeight = $(window).height();
        var documentHeight = $(document).height();

        if (scrollPos + windowHeight >= documentHeight) {
          $('#myFooter').removeClass('d-none');
        } else {
          $('#myFooter').addClass('d-none');
        }
      });

      var prevScrollPos = $(window).scrollTop();
      var navbar = $("#navbar");
      $(window).scroll(function () {
        var currentScrollPos = $(window).scrollTop();
        if (prevScrollPos > currentScrollPos) {
          navbar.removeClass("hide");
        } else {
          navbar.addClass("hide");
          $('.navbar-collapse').collapse('hide');
        }
        prevScrollPos = currentScrollPos;
      });

      var filter = 'all';
      function fetchData(category) {
        return fetch('/ifc-ior/static/data/team.json')
          .then(response => response.json())
          .then(data => {
            var year = $('#year').text();
            data = data[year];
            if (category === 'all') {
              return Object.values(data).flatMap(Object.values);
            } else {
              return data[category] || [];
            }
          })
          .catch(error => {
            console.error('Error:', error);
            return [];
          });
      }

      function populateGallery(items) {
        for (var i = 0; i < items.length; i++) {
          var caption = items[i][0];
          var description = items[i][1];
          var imagePath = items[i][2];
          var category = items[i][3];
          var service = items[i][4];

          if (i % 4 === 0 || i === items.length) {
            row = $('<div>').addClass('row');
            $('#gallery').append(row);
          }

          var card = $('<div>').addClass('card mx-auto my-2').attr('category', category);
          var cardImage = $('<img>').addClass('card-img-top').attr('src', imagePath).attr('alt', caption).attr('data-index', i);
          var cardBody = $('<div>').addClass('card-body');
          var cardCaption = $('<h6>').addClass('card-title text-center mt-2').text(caption);
          var cardDescription = $('<p>').addClass('card-text text-center').html(description);
          var serviceDesc = $('<p>').addClass('card-text text-center mt-0').html(service);
          cardBody.append(cardCaption)
          cardBody.append(serviceDesc);
          cardBody.append(cardDescription);
          card.append(cardImage).append(cardBody);
          row.append(card);
        }
      }

      function updateGallery() {
        fetchData(filter)
          .then(items => {
            $('#gallery').empty();
            populateGallery(items);
          })
          .catch(error => {
            console.error('Error:', error);
          });
      }

      fetch('/ifc-ior/static/data/team.json')
        .then(response => response.json())
        .then(data => {
          var year = $('#year').text();
          var categoryItems = data[year];
          var items = [];
          for (let key in categoryItems) {
            var category = categoryItems[key];
            for (let i = 0; i < category.length; i++) {
              items.push(category[i]);
            }
          }
          populateGallery(items);
        })
        .catch(error => {
          console.error('Error:', error);
        });

      $('#filterButtons button').click(function () {
        $('#filterButtons button').removeClass('active');
        $(this).addClass('active');
        filter = $(this).attr('data-filter');

        updateGallery();
      });

      $('#filterDropdown').change(function () {
        filter = $('.form-select').val();
        updateGallery();
      });
    }

  };
}();

jQuery(document).ready(function () {
  Archive.init();
});
