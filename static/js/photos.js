var Photos = function () {
  var currentImageIndex = 0;
  var prevScrollPos = $(window).scrollTop();
  var navbar = $("#navbar");
  $(window).scroll(function () {
    var currentScrollPos = $(window).scrollTop();
    if (prevScrollPos > currentScrollPos) {
      navbar.removeClass("hide");
    } else {
      navbar.addClass("hide");
      $('.navbar-collapse').collapse('hide');
    }
    prevScrollPos = currentScrollPos;
  });

  return {
    init: function () {
      $(window).scroll(function () {
        var scrollPos = $(document).scrollTop();
        var windowHeight = $(window).height();
        var documentHeight = $(document).height();

        if (scrollPos + windowHeight >= documentHeight) {
            $('#myFooter').removeClass('d-none');
        } else {
            $('#myFooter').addClass('d-none');
        }
      });
      fetch('/ifc-ior/static/data/photos.json')
        .then(response => response.json())
        .then(data => {
          var year = $('#year').text();
          var items = data[year];
          var row;

          for (var i = 0; i < items.length; i++) {
            var caption = items[i][0];
            var imagePath = items[i][1];
            if (i % 4 === 0) {
              row = $('<div>').addClass('row');
              $('#gallery').append(row);
            }

            var col = $('<div>').addClass('col-md-3 p-0 my-2 text-center');
            var card = $('<div>').addClass('card mx-auto my-auto');
            var cardImage = $('<img>').addClass('card-img-top').attr('src', imagePath).attr('alt', caption).attr('data-index', i);
            var cardBody = $('<div>').addClass('card-body');
            var cardCaption = $('<h6>').addClass('card-title caption-center').text(caption);

            cardBody.append(cardCaption);
            card.append(cardImage).append(cardBody);
            col.append(card);
            row.append(col);
          }

          $('.card-img-top').click(function () {
            var imageModal = $('#imageModal');

            var clickedImage = $(this);
            var imageSrc = clickedImage.attr('src');
            var imageAlt = clickedImage.attr('alt');
            var caption = clickedImage.attr('alt');

            imageModal.find('.modal-body img').attr('src', imageSrc);
            imageModal.find('.modal-body img').attr('alt', imageAlt);
            imageModal.find('.modal-body .caption').text(caption);

            imageModal.modal('show');
          });
        })
        .catch(error => {
          console.error('Error:', error);
        });
    }
  };
}();

jQuery(document).ready(function () {
  Photos.init();
});
