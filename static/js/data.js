// Custom date parsing function
function parseDate(dateString) {
    const parts = dateString.split('/');
    const day = parseInt(parts[0], 10);
    const month = parseInt(parts[1], 10) - 1; // Month is zero-based in JavaScript
    const year = parseInt(parts[2], 10);
    return new Date(year, month, day);
}

//Handling Incidents
document.addEventListener("DOMContentLoaded", function () {
    fetch('/ifc-ior/static/data/incidents.json')
        .then(response => response.json())
        .then(jsonData => {
            const limitedData = jsonData.slice(0, 11);
            const incidentScroll = `
                          <div class="incidents mt-2" style="overflow-y: scroll; height=""></div>
                          <a class="btn btn-primary mt-2" href="/ifc-ior/incidents.html">View More</a>`;

            const incidentMarquee = document.querySelector('#incidentsCard');
            incidentMarquee.insertAdjacentHTML('beforeend', incidentScroll);

            const container = document.querySelector('.incidents');
            limitedData.forEach((item, index) => {
                const card = document.createElement('div');
                card.classList.add('card3');

                const link = document.createElement('a');
                link.href = item.link;
                link.target = "_blank";

                const cardBody = document.createElement('div');
                cardBody.classList.add('card-body');

                const incident = document.createElement('div');
                incident.classList.add('incident');
                incident.textContent = item.incident;

                const date = document.createElement('div');
                date.style.fontSize = '0.7rem';
                date.style.fontWeight = 'lighter';
                date.textContent = item.date;

                // Construct the HTML structure
                incident.appendChild(date);
                cardBody.appendChild(incident);
                link.appendChild(cardBody);
                card.appendChild(link);
                container.appendChild(card);

                // Add event listeners for hover effect
                card.addEventListener("mouseover", function () {
                    container.querySelectorAll(".card3").forEach((c, i) => {
                        if (i !== index) {
                            c.classList.add("faded");
                        }
                    });
                });

                card.addEventListener("mouseout", function () {
                    container.querySelectorAll(".card3").forEach((c, i) => {
                        if (i !== index) {
                            c.classList.remove("faded");
                        }
                    });
                });
            });
        })
        .catch(error => {
            console.error('Error fetching JSON data:', error);
        });
});

//Handling Events
document.addEventListener("DOMContentLoaded", function () {
    fetch('/ifc-ior/static/data/events.json')
      .then(response => response.json())
      .then(jsonData => {
          const limitedData = jsonData.slice(0, 10);
          const eventScroll = `
                     <div class="events mt-2" style="overflow-y: scroll; height=""></div>            
                     <a class="btn btn-primary mt-2" href="/ifc-ior/events.html">View More</a>`;
         
          const eventMarquee = document.querySelector('#eventsCard');
          eventMarquee.insertAdjacentHTML('beforeend', eventScroll);
  
          const container = document.querySelector('.events');
          limitedData.forEach((item, index) => {
              const card = document.createElement('div');
              card.classList.add('card3');
  
              const link = document.createElement('a');
              link.href = item.link;
  
              const cardBody = document.createElement('div');
              cardBody.classList.add('card-body');
  
              const event = document.createElement('div');
              event.classList.add('event');
              event.textContent = item.event;
  
              const date = document.createElement('div');
              date.style.fontSize = '0.7rem';
              date.style.fontWeight = 'lighter';
              date.textContent = item.date;
  
              // Construct the HTML structure
              event.appendChild(date);
              cardBody.appendChild(event);
              link.appendChild(cardBody);
              card.appendChild(link);
              container.appendChild(card);
  
              // Add event listeners for hover effect
              card.addEventListener("mouseover", function () {
                  container.querySelectorAll(".card3").forEach((c, i) => {
                      if (i !== index) {
                          c.classList.add("faded");
                      }
                  });
              });
  
              card.addEventListener("mouseout", function () {
                  container.querySelectorAll(".card3").forEach((c, i) => {
                      if (i !== index) {
                          c.classList.remove("faded");
                      }
                  });
              });
          });
      })
      .catch(error => {
          console.error('Error fetching JSON data:', error);
      });
  });
  
