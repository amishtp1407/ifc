var navbar1 = $("#navbar");
var prevScrollPos = $(window).scrollTop();
$(window).scroll(function () {
  var currentScrollPos = $(window).scrollTop();
  if (prevScrollPos > currentScrollPos) {
    navbar1.removeClass("hide");
  } else {
    navbar1.addClass("hide");
    $('.navbar-collapse').collapse('hide');
  }

  prevScrollPos = currentScrollPos;
});
